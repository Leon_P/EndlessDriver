package model;

public class Car {



    // Eigenschaften
    private int x;
    private int y;
    private int h;
    private int w;

    // Konstruktoren
    public Car(int x) {
        this.x = x;
        this.y = (int)(Model.HEIGHT*.9);
        this.h = 30;
        this.w = 30;
    }

    // Methoden
    public void move (int dx) {
        this.x += dx;
    }


    // Getter + Setter
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public void setX(int x) {
        this.x = x;
    }

}
