package model;


import javafx.scene.paint.Color;

public class StreetTile {

    // Eigenschaften
    private int x;
    private int y;
    private int h;
    private int w;
    private int street;
    private float speed;
    private javafx.scene.paint.Color side;


    // Konstruktoren
    public StreetTile(int x, int y, int w) {
        this.x = x;
        this.y = y;
        this.h = 3;
        this.w = w;
        this.speed = 1;

        if (Model.isRedToWhite())
            this.side = Color.SADDLEBROWN.brighter();
        else
            this.side = Color.SADDLEBROWN;
    }

    // Methoden
    public void update(long elapsedTime){
        street = (int)Math.round(w*.07);
        this.w+=street;
        this.speed+=1;
        if (speed%3==0) {
            this.h += 1;
            this.y -=1;
        }
        this.y += h;
    }

    // Setter + Getter
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public javafx.scene.paint.Color getSide() {
        return side;
    }
}
