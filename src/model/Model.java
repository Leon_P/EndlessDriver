package model;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Model extends Component {

    // FINALS
    public static final int WIDTH = 1000;
    public static final int HEIGHT = 800;
    public static final int SWIDTH = 250;


    // Eigenschaften
    private List<StreetTile> streetTiles = new LinkedList<>();
    private List<Background> backgrounds = new LinkedList<>();
    private Car car;
    private StreetTile currentStreetTile = new StreetTile((WIDTH / 2), (int) (HEIGHT * .4), (int) (SWIDTH * .05));
    private Background currentBG = new Background(1,HEIGHT);
    private Random rnd = new Random();
    private int redToWhiteCounter = 0;
    private static int score;
    private static int gameStatus = 0;



    private static boolean redToWhite;


    // Konstruktoren
    public Model() {
        this.car = new Car(WIDTH / 2);
        streetTiles.add(currentStreetTile);
        backgrounds.add(currentBG);
    }

    // Methoden


    public void startUpdate(long elapsedTime){
        score=0;

        for (StreetTile streetTile : streetTiles) {
            streetTile.update(elapsedTime);
        }
        if (currentStreetTile.getY() >= HEIGHT * .4) {
            if(currentStreetTile.getX()<WIDTH/2)
                currentStreetTile = new StreetTile(currentStreetTile.getX()+3, currentStreetTile.getY() - currentStreetTile.getH(), (int) (SWIDTH * .05));
            else if (currentStreetTile.getX()>WIDTH/2)
                currentStreetTile = new StreetTile(currentStreetTile.getX()-3, currentStreetTile.getY() - currentStreetTile.getH(), (int) (SWIDTH * .05));
            else
                currentStreetTile = new StreetTile(currentStreetTile.getX(), currentStreetTile.getY() - currentStreetTile.getH(), (int) (SWIDTH * .05));


            streetTiles.add(currentStreetTile);
        }
        if (streetTiles.get(0).getY() > HEIGHT)
            streetTiles.remove(0);
    }

    public void update(long elapsedTime) {

        redToWhite();
        newRandomStreetTile();
        removeTilesAndAddScore();
        checkCarOnStreet(elapsedTime);
        updateBG();

    }
    public void updateBG(){
        for (Background bg : backgrounds) {
            bg.update();
        }
        if(currentBG.getY()<=0){
            currentBG = new Background((int)(currentBG.getH()),currentBG.getH());
            backgrounds.add(currentBG);
        }
        if(backgrounds.get(0).getY()<=-(currentBG.getH())) {
            backgrounds.remove(0);
        }
    }

    private void checkCarOnStreet(long elapsedTime) {
        for (StreetTile streetTile : streetTiles) {
            streetTile.update(elapsedTime);

            if (streetTile.getY() >= car.getY() && streetTile.getY() < (car.getY() + car.getH())) {

                boolean leftStreetBorder = car.getX() < streetTiles.get(5).getX()-streetTiles.get(5).getW()/2;//(streetTile.getX() - (SWIDTH / 2) - SWIDTH * .1);
                boolean rightStreetBorder = (car.getX()) > streetTiles.get(5).getX() + (streetTiles.get(5).getW() / 2) * 1.1;

                if (leftStreetBorder || rightStreetBorder){
                    gameStatus = 2;

                }
            }
        }
    }

    private void removeTilesAndAddScore() {

        if (streetTiles.get(0).getY() > HEIGHT){
            streetTiles.remove(0);
            score+=1;
        }
    }

    private void newRandomStreetTile() {
        float threshold = 2f;

        int newX;
        if (currentStreetTile.getY() >= HEIGHT * .4) {
            if (currentStreetTile.getX() > SWIDTH && ((currentStreetTile.getX() +
                    currentStreetTile.getW()) < WIDTH - SWIDTH)) { //default
                newX = currentStreetTile.getX() + rnd.nextInt((int) (currentStreetTile.getW() * threshold))
                        - (rnd.nextInt((int) (currentStreetTile.getW() * threshold)));
            } else if (currentStreetTile.getX() - (currentStreetTile.getW() / 2) <= SWIDTH) {//linker Rand
                newX = currentStreetTile.getX() + rnd.nextInt((int) (currentStreetTile.getW() * threshold));
            } else {
                newX = currentStreetTile.getX() - rnd.nextInt((int) (currentStreetTile.getW() * threshold));
            }
            currentStreetTile = new StreetTile((newX), currentStreetTile.getY()
                    - currentStreetTile.getH(), (int) (SWIDTH * .05));
            streetTiles.add(currentStreetTile);
//
        }
    }

    private void redToWhite() {
        redToWhiteCounter++;
        if (redToWhiteCounter == 4) {
            redToWhiteCounter = 0;
            redToWhite = !redToWhite;
        }
    }

    // Setter + Getter
    public List<StreetTile> getStreetTiles() {
        return streetTiles;
    }

    public Car getCar() {
        return car;
    }

    public static int getGameStatus() {
        return gameStatus;
    }

    public static void setGameStatus(int gameStatus) {
        Model.gameStatus = gameStatus;
    }
    public static int getScore() {
        return score;
    }
    public static boolean isRedToWhite() {
        return redToWhite;
    }

    public Background getCurrentBG() {
        return currentBG;
    }
    public List<Background> getBackgrounds() {
        return backgrounds;
    }
}
