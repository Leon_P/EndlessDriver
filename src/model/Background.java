package model;

public class Background {
    private int h;
    private int y;
    private int speed;



    public Background(int y, int h) {
        this.h = h;
        this.y = y;

    }

    public void update(){
        speed++;
        if(speed==7) {
            y -= 1;
            speed = 0;
        }
    }
    public int getH() {
        return h;
    }

    public int getY() {
        return y;
    }

}
