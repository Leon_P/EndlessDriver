import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import model.Background;
import model.Model;
import model.StreetTile;

public class Graphics {

    // Eigenschaften
    private Model model;
    private GraphicsContext gc;


    // Konstruktoren
    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }
    //Start Draw
    public void start() {
        draw();
        drawCar();

        gc.setFill(Color.WHITE);
        gc.setFont(new Font("Arial", 75));
        gc.setTextAlign(TextAlignment.CENTER);

        gc.fillText("PRESS "+(char)0X23CE+" TO START", Model.WIDTH/2, Model.HEIGHT/4);

    }

    // Methoden
    public void drawDefault() {
        draw();
        // Draw Player
        drawCar();
        gc.setFill(Color.WHITE);
        gc.setFont(new Font("Arial", 50));
        gc.setTextAlign(TextAlignment.LEFT);
        gc.setFill(Color.WHITE);
        gc.setFont(new Font("Arial", 75));
        gc.setTextAlign(TextAlignment.CENTER);
        gc.fillText("Score:"+Model.getScore(), Model.WIDTH/2, Model.HEIGHT/4);
    }



    public void gameOverScreen() {
        Image image = new Image("Ressources/CarDead.png");
        draw();
        gc.setFill(new ImagePattern(image));
        gc.fillRect(
                model.getCar().getX() - 5,
                model.getCar().getY() - 15,
                model.getCar().getW() * 3.5,
                model.getCar().getH() * 3.5
        );
        gc.setFill(Color.WHITE);
        gc.setFont(new Font("Arial", 75));
        gc.setTextAlign(TextAlignment.CENTER);
        gc.fillText("GAME OVER\nPRESS \"R\" TO RESTART\n"+Model.getScore()+
                " Points!", Model.WIDTH/2, Model.HEIGHT/6);
    }

    public void drawCar(){
        Image image = new Image("Ressources/CarAlive.png");
        gc.setFill(Color.BLACK);
        gc.setFill(new ImagePattern(image));
        gc.fillRect(
                model.getCar().getX() - 15,
                model.getCar().getY() - 15,
                model.getCar().getW() * 2,
                model.getCar().getH() * 2
        );
    }

    public void draw() {
        // Clear Screen
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);

        for (Background b : this.model.getBackgrounds()) {
            Image image = new Image("Ressources/bg.jpg");
            gc.setFill(Color.BLACK);
            gc.setFill(new ImagePattern(image));
            gc.fillRect(0,b.getY(), Model.WIDTH, b.getH());
        }

        gc.setFill(Color.DARKGREEN.darker());
        gc.fillOval(-Model.WIDTH * 1.5, Model.HEIGHT * 0.4, Model.WIDTH * 4, Model.HEIGHT * 0.8);

        for (StreetTile streetTile : this.model.getStreetTiles()) {
            gc.setFill(Color.DARKGRAY.darker());
            gc.fillRect(
                    streetTile.getX() - streetTile.getW() / 2,
                    streetTile.getY(),
                    streetTile.getW(),
                    streetTile.getH()
            );

            Color c = streetTile.getSide();
            gc.setFill(c);
            gc.fillRect(
                    streetTile.getX() - streetTile.getW() / 2,
                    streetTile.getY(),
                    streetTile.getW() / 10,
                    streetTile.getH()
            );

            gc.fillRect(
                    streetTile.getX() + streetTile.getW() / 2,
                    streetTile.getY(),
                    streetTile.getW() / 10,
                    streetTile.getH()
            );
        }
    }
}
