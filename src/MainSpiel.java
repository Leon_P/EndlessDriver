import javafx.application.Application;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;
import model.Model;

import java.awt.*;


public class MainSpiel extends Application {

    // Eigenschaften initialisieren
    private Timer timer;


    // Konstruktoren

    // Methoden


    public static void resCursor() {
        try {
            Robot robot = new Robot();
            robot.mouseMove((int) Screen.getPrimary().getBounds().getWidth() / 2, 700);
        } catch (AWTException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage stage) throws Exception {



        /* javaFX Vorbereitungen / Stage
        /////////////////////////////////*/

        // Canvas
        Canvas canvas = new Canvas(Model.WIDTH, Model.HEIGHT);

        // Group
        Group group = new Group();
        group.getChildren().add(canvas);

        //Scene
        Scene scene = new Scene(group);
        scene.setCursor(Cursor.NONE);

        //cursorPosition
        resCursor();

        // Stage
        stage.setScene(scene);
        stage.show();
        stage.getIcons().add(new Image("Ressources/CarAlive.png"));
        stage.setTitle("Endless Race");

        //Sound

        // Draw
        GraphicsContext gc = canvas.getGraphicsContext2D();
        Model model = new Model();
        Graphics graphics = new Graphics(model, gc);
        timer = new Timer(model, graphics);
        timer.start();

        // InputHandler
        InputHandler inputHandler = new InputHandler(model);
        scene.setOnKeyPressed(
                event -> {
                    try {
                        inputHandler.onKeyPressed(event.getCode());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
        );

            canvas.setOnMouseMoved(
                event -> inputHandler.onMouseMove(
                        (int) event.getX()
                        //(int)event.getY()
                )
        );
    }

    @Override
    public void stop() throws Exception {
        timer.stop();
        super.stop();
    }
}
