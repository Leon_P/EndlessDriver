import javafx.scene.input.KeyCode;
import model.Model;

public class InputHandler {

    // Eigenschaften
    private Model model;

    // Konstruktoren
    public InputHandler(Model model) {
        this.model = model;
    }

    // Methoden
    public void onKeyPressed(KeyCode key) throws Exception {
        if (key == KeyCode.ENTER) {
            Model.setGameStatus(1);
        } else if (key == KeyCode.R) {
            if (Model.getGameStatus() == 2)
                Model.setGameStatus(0);
        }
    }


    public void onMouseMove(int x) {
        if(Model.getGameStatus()==1)
            model.getCar().move(
                x - model.getCar().getX()

        );
    }

}
